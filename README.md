# veegilmedia-banking-app

## Odusina Olutola Samuel

## Getting started

The project is about a banking application for both mobile and web.

Here is the link to my submission [veegilmedia](https://www.figma.com/file/AKm9dKJIJqbPhumEENiYQY/Veegil-Media?node-id=306%3A3626&t=bbbPtkS9mxCnEmGR-1)!

## Sign Up and Log In Screen
This is the screen where users can sign up and login to their account. After successful account creation, a verify email code is sent to the user and also a verify phone number code is sent the phone number of the users, the user phone number needs to be validated as it serves as the users account number.

![Sign Up and Login Screen](https://gitlab.com/Olutola/veegilmedia-banking-app/uploads/92d2bef36b06904167696159f9d9f427/SignUpAndLoginScreen.png)

## Home, Deposit and Transfer Screen
This provides functionality for users to be able to transfer money to other bank accounts. Users can also credit their account likewise.

![Home and Deposit Screen ](https://gitlab.com/Olutola/veegilmedia-banking-app/uploads/ead67b5cfb13f73642ece9a82807ffcd/DepositandTransferScreen.png)

## Transaction Screen
Users can be able to view all transaction histories for both credit and debit alert,and also share payment receipt to other users.

![Transaction](https://gitlab.com/Olutola/veegilmedia-banking-app/uploads/3193fd845b06fd38b78f26e8f518966c/TransactionAndReceiptHistory.png)

## Web Browser Landing Page
The web browser shows the activities of the bank, on this web page, all the action buttons and action words leads directly to your apple store and google play store, for you to download the app and sign up.

![Landing Page](https://gitlab.com/Olutola/veegilmedia-banking-app/uploads/64effacbe90b78bdeca699599f113771/Web_Landing_Page.png)







